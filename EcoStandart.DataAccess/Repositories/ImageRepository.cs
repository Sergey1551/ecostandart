﻿using EcoStandart.DataAccess.Entities;
using EcoStandart.DataAccess.Interfaces;

namespace EcoStandart.DataAccess.Repositories
{
    public class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        public ImageRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
