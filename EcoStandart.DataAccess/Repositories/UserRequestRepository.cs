﻿using EcoStandart.DataAccess.Entities;
using EcoStandart.DataAccess.Interfaces;

namespace EcoStandart.DataAccess.Repositories
{
    public class UserRequestRepository : GenericRepository<UserRequest>, IUserRequestRepository
    {
        public UserRequestRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
