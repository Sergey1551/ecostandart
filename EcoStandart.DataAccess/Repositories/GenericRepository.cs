﻿using EcoStandart.DataAccess.Entities;
using EcoStandart.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EcoStandart.DataAccess.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApplicationContext _context;
        protected readonly DbSet<TEntity> _table;

        public GenericRepository(ApplicationContext context)
        {
            _context = context;
            _table = context.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> Get()
        {
            var item = await _table.AsNoTracking().ToListAsync();

            return item;
        }
        
        public async Task<TEntity> Get(long id)
        {
            var item = await _table.FirstOrDefaultAsync(x => x.Id == id);

            return item;
        }

        public async Task<long> Add(TEntity item)
        {
            var result = await _table.AddAsync(item);
            _context.SaveChanges();

            return result.Entity.Id;
        }

        public async Task Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task Remove(TEntity item)
        {
            _table.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task Remove(IEnumerable<TEntity> items)
        {
            _table.RemoveRange(items);
            await _context.SaveChangesAsync();
        }
    }
}
