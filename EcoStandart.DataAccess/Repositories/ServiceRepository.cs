﻿using EcoStandart.DataAccess.Entities;
using EcoStandart.DataAccess.Interfaces;

namespace EcoStandart.DataAccess.Repositories
{
    public class ServiceRepository : GenericRepository<Service>, IServiceRepository
    {
        public ServiceRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
