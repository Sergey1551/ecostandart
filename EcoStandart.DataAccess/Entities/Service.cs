﻿using System.Collections.Generic;

namespace EcoStandart.DataAccess.Entities
{
    public class Service : BaseEntity
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string Heading { get; set; }
        public string Body { get; set; }
    }
}