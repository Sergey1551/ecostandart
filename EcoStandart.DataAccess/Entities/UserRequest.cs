﻿namespace EcoStandart.DataAccess.Entities
{
    public class UserRequest : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string RequestSubject { get; set; }
    }
}
