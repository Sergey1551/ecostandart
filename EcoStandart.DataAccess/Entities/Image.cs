﻿namespace EcoStandart.DataAccess.Entities
{
    public class Image : BaseEntity
    {
        public string ContentType { get; set; }
        public string Extention { get; set; }
        public string Name { get; set; }
        public string Folder { get; set; }
    }
}
