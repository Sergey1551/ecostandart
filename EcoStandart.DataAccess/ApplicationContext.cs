﻿using EcoStandart.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace EcoStandart.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
              : base(options)
        {
            Database.EnsureCreated();
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserRequest>()
                .HasKey(x => x.Id);
            builder.Entity<Service>()
                .HasKey(x => x.Id);   
            builder.Entity<Image>()
                .HasKey(x => x.Id);            
        }

        public DbSet<UserRequest> UserRequests { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Image> Images { get; set; }
    }
}

