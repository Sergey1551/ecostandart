﻿using EcoStandart.DataAccess.Entities;

namespace EcoStandart.DataAccess.Interfaces
{
    public interface IImageRepository : IGenericRepository<Image>
    {
    }
}
