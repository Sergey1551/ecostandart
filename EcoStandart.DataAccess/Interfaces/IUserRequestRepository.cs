﻿using EcoStandart.DataAccess.Entities;

namespace EcoStandart.DataAccess.Interfaces
{
    public interface IUserRequestRepository : IGenericRepository<UserRequest>
    {
    }
}
