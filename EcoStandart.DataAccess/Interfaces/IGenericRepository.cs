﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EcoStandart.DataAccess.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<long> Add(TEntity item);
        Task<IEnumerable<TEntity>> Get();
        Task<TEntity> Get(long id);
        Task Remove(TEntity item);
        Task Remove(IEnumerable<TEntity> items);
        Task Update(TEntity item);
    }
}
