﻿using EcoStandart.DataAccess.Entities;

namespace EcoStandart.DataAccess.Interfaces
{
    public interface IServiceRepository : IGenericRepository<Service>
    {
    }
}
