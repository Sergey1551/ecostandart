﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.BusinessLogic.ViewModels.ImageViews;
using EcoStandart.DataAccess.Entities;
using EcoStandart.DataAccess.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services
{
    public class ImageService : IImageService
    {

        private readonly IImageRepository _imageRepository;

        public ImageService(IImageRepository imageRepository)
        {
            _imageRepository = imageRepository;
        }

        public Task<long> Upload(UploadImageView request)
        {
            if (!Directory.Exists($"{Directory.GetCurrentDirectory()}/images"))
            {
                Directory.CreateDirectory($"{Directory.GetCurrentDirectory()}/images");
            }

            var image = new Image();
            image.ContentType = request.File.ContentType;
            image.Extention = request.File.FileName.Split(".").LastOrDefault();
            image.Folder = $"{Directory.GetCurrentDirectory()}/images/{Guid.NewGuid()}";
            image.Name = request.File.FileName;

            using (var fileStream = new FileStream(image.Folder, FileMode.Create))
            {
                request.File.CopyTo(fileStream);
            }

            return _imageRepository.Add(image);
        }

        public async Task<(Stream fileStream, string contentType, string fileName)> Get(long id)
        {
            var image = await _imageRepository.Get(id);

            var fileStream = new FileStream(image.Folder, FileMode.Open, FileAccess.Read);

            return (fileStream, image.ContentType, image.Name);

        }
    }
}
