﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.BusinessLogic.ViewModels.UserRequestViews;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;

        public EmailService(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
            EmailSettings = emailSettings;
        }

        public IOptions<EmailSettings> EmailSettings { get; }

        public Task SendEmailAsync(AddUserRequestView model)
        {
            try
            {                                               
                var credentials = new NetworkCredential(_emailSettings.Email, _emailSettings.Password);
                var body = "Пользователь по имени " + model.Name;
                if(!string.IsNullOrEmpty(model.RequestSubject))
                {
                    body += " задал вопрос: " + model.RequestSubject;
                }
                if (string.IsNullOrEmpty(model.RequestSubject))
                {
                    body += " Заказал консультацию. " + model.RequestSubject;
                }
                if (!string.IsNullOrEmpty(model.PhoneNumber))
                {
                    body += " Номер телефона: " + model.PhoneNumber;
                }
                if (!string.IsNullOrEmpty(model.Email))
                {
                    body += " Электронная почта: " + model.Email;
                }

                var mail = new MailMessage()
                {
                    From = new MailAddress(_emailSettings.Email, _emailSettings.SenderName),
                    Subject = "Пользователь " + model.Name + " оставил вопрос.",
                    Body = body,    
                    IsBodyHtml = true
                };

                mail.To.Add(new MailAddress(_emailSettings.Email));

                var client = new SmtpClient()
                {
                    Port = _emailSettings.MailPort,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = _emailSettings.MailServer,
                    EnableSsl = true,   
                    Credentials = credentials
                };

                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
