﻿using EcoStandart.BusinessLogic.ViewModels.ImageViews;
using System.IO;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services.Interfaces
{
    public interface IImageService
    {
        Task<long> Upload(UploadImageView request);
        Task<(Stream fileStream, string contentType, string fileName)> Get(long id);
    }
}
