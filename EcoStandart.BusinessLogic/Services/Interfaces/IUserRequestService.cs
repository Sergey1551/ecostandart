﻿using EcoStandart.BusinessLogic.ViewModels.UserRequestViews;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services.Interfaces
{
    public interface IUserRequestService
    {
        Task AddUserRequest(AddUserRequestView model);
    }
}
