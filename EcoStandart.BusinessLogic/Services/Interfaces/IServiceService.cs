﻿using EcoStandart.BusinessLogic.ViewModels.UserRequestViews;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services.Interfaces
{
    public interface IServiceService
    {
        Task<IEnumerable<GetListOfServicesView>> GetListOfServices();
        Task<GetServiceView> GetService(long id);
    }
}
