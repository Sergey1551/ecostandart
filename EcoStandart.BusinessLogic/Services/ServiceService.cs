﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.BusinessLogic.ViewModels.UserRequestViews;
using EcoStandart.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services
{
    public class ServiceService : IServiceService
    {

        private readonly IServiceRepository _serviceRepository;

        public ServiceService(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public async Task<IEnumerable<GetListOfServicesView>> GetListOfServices()
        {
            var listOfServices = (await _serviceRepository.Get()).Select(x => new GetListOfServicesView
            {
                Id = x.Id,
                Name = x.Name + " " + x.Abbreviation
            });

            return listOfServices;
        }

        public async Task<GetServiceView> GetService(long id)
        {
            var service = await _serviceRepository.Get(id);
            var result = new GetServiceView()
            {
                Body = service.Body,
                Heading = service.Heading,
                Name = service.Name
            };

            return result;
        }
    }
}
