﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.BusinessLogic.ViewModels.UserRequestViews;
using EcoStandart.DataAccess.Entities;
using EcoStandart.DataAccess.Interfaces;
using System.Threading.Tasks;

namespace EcoStandart.BusinessLogic.Services
{
    public class UserRequestService : IUserRequestService
    {
        private readonly IUserRequestRepository _userRequestRepository;
            
        public UserRequestService(IUserRequestRepository userRequestRepository)
        {
            _userRequestRepository = userRequestRepository;
        }

        public async Task AddUserRequest(AddUserRequestView model)
        { 
            var newUserRequest = new UserRequest
            {
                Name = model.Name,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                RequestSubject = model.RequestSubject,
            };

            await _userRequestRepository.Add(newUserRequest);

            return;
        }
    }
}
