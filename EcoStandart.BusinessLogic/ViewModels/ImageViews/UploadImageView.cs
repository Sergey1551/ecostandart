﻿using Microsoft.AspNetCore.Http;

namespace EcoStandart.BusinessLogic.ViewModels.ImageViews
{
    public class UploadImageView
    {
        public IFormFile File { get; set; }
        public long ServiceId { get; set; }
    }
}
