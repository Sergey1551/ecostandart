﻿using System.Collections.Generic;

namespace EcoStandart.BusinessLogic.ViewModels.UserRequestViews
{
    public class GetServiceView
    {
        public string Name { get; set; }
        public string Heading { get; set; }
        public string Body { get; set; }
    }
}
