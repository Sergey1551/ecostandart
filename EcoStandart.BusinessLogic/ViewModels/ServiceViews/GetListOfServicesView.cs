﻿namespace EcoStandart.BusinessLogic.ViewModels.UserRequestViews
{
    public class GetListOfServicesView
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
