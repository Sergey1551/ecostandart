﻿using System.ComponentModel.DataAnnotations;

namespace EcoStandart.BusinessLogic.ViewModels.UserRequestViews
{
    public class AddUserRequestView
    {
        [Required]
        public string Name { get; set; }
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
        public string RequestSubject { get; set; }
    }
}
