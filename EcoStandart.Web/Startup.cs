using EcoStandart.BusinessLogic;
using EcoStandart.BusinessLogic.Services;
using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.DataAccess;
using EcoStandart.DataAccess.Interfaces;
using EcoStandart.DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EcoStandart.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(option => option.EnableEndpointRouting = false);

            // Email send
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));

            // Connection String
            services.Configure<ConfigSettings>(Configuration.GetSection("ConfigSettings"));

            services.AddDbContext<ApplicationContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Repositories
            services.AddScoped<IUserRequestRepository, UserRequestRepository>();
            services.AddScoped<IServiceRepository, ServiceRepository>();
            services.AddScoped<IImageRepository, ImageRepository>();

            // Services
            services.AddScoped<IUserRequestService, UserRequestService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IImageService, ImageService>();

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins()
                                                          .AllowAnyHeader()
                                                          .AllowAnyMethod();
                                  });
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseMvc();
        }
    }
}
