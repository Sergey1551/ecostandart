﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.BusinessLogic.ViewModels.UserRequestViews;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EcoStandart.Web.Controllers
{
    [Route("api/userRequest")]
    [ApiController]
    public class UserRequestController : ControllerBase
    {
        private readonly IUserRequestService _userRequestService;
        private readonly IEmailService _emailService;

        public UserRequestController(IUserRequestService productService,
                                     IEmailService emailService)
        {
            _userRequestService = productService;
            _emailService = emailService;
        }

        [HttpPost]
        [Route("addUserRequest")]
        public async Task<IActionResult> AddUserRequest(AddUserRequestView model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _emailService.SendEmailAsync(model);

            await _userRequestService.AddUserRequest(model);

            return Ok();
        }
    }
}
