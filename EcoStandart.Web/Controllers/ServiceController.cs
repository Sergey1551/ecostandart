﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EcoStandart.Web.Controllers
{

    [Route("api/service")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceService _serviceService;

        public ServiceController(IServiceService serviceService)
        {
            _serviceService = serviceService;
        }

        [HttpGet]
        [Route("getListOfServices")]
        public async Task<IActionResult> GetListOfServices()
        {
            var result = await _serviceService.GetListOfServices();

            return Ok(result);
        }

        [HttpGet]
        [Route("getService/{id}")]
        public async Task<IActionResult> GetService(long id)
        {
            var result = await _serviceService.GetService(id);

            return Ok(result);
        }
    }
}