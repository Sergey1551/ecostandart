﻿using EcoStandart.BusinessLogic.Services.Interfaces;
using EcoStandart.BusinessLogic.ViewModels.ImageViews;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace EcoStandart.Web.Controllers
{
    [Route("api/image")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IImageService _imageService;

        public ImageController(IImageService imageService)

        {
            _imageService = imageService;
        }

        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> Upload([FromForm]UploadImageView request)
        {
            var result = await _imageService.Upload(request);

            return Ok(result);     
        }

        [HttpGet]
        [Route("get/{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var result = await _imageService.Get(id);

            return File(result.fileStream, result.contentType, result.fileName);       
        }

    }
}
