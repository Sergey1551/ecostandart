export const environment = {
  production: true,
  baseUrl:"http://localhost:53142",
  userRequestUrl:"http://localhost:53142/api/userRequest/",
  serviceUrl:"http://localhost:53142/api/service/"
};