import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserRequest } from '../shared/interfaces/user-request';
import { ServiceService } from '../shared/services/service.service';
import { UserRequestService } from '../shared/services/user-request.service';
import { ServiceList } from '../shared/interfaces/service-list';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

    addUserRequestForm: FormGroup;
    name: FormControl;
    email: FormControl;
    phoneNumber: FormControl;
    requestSubject: FormControl;
    page: Page = 1;
    isDesktop: boolean = false;
    isMobile: boolean = false;
    isTablet: boolean = false;
    deviceInfo = null;
    listOfServices: ServiceList[] = [];
    isAgree: boolean = false;
    lat = 56.8431;
    lng = 60.6454;
    isDropdown: boolean = false;

    constructor(
        private userRequestService: UserRequestService,
        private serviceService: ServiceService,
        private formBuilder: FormBuilder,
        private deviceService: DeviceDetectorService) { }

    ngOnInit() {
        this.isDesktop = this.deviceService.isDesktop();
        this.isMobile = this.deviceService.isMobile();
        this.isTablet = this.deviceService.isTablet();
        
        // this.getListOfServices().subscribe(result => {
        //     this.listOfServices = result;
        // });
        this.name = new FormControl('', [Validators.required, Validators.maxLength(50)]);
        this.email = new FormControl('', [Validators.required, Validators.maxLength(50)]);
        this.phoneNumber = new FormControl('', [Validators.required, Validators.maxLength(50)]);
        this.requestSubject = new FormControl('', [Validators.required, Validators.minLength(25), Validators.maxLength(500)]);

        this.addUserRequestForm = this.formBuilder.group({
            'name': this.name,
            'email': this.email,
            'phoneNumber': this.phoneNumber,
            'requestSubject': this.requestSubject,
        });
    }

    chooseService(page: Page) {
        this.page = page;     
        this.isDropdown = false;   
    }

    addUserRequest() {
        let userRequst = this.addUserRequestForm.value
        this.addUserRequestForm.reset();
        this.userRequestService.addUserRequest(userRequst).subscribe(
            result => {
                console.log("User request added successfully")
            },
            error => {
                console.log("Error")
            }
        );
    }

    isServiceDropdown() {
        if(this.isDropdown)
        {
            this.isDropdown = false;
            return;
        }
        if(!this.isDropdown)
        {
            this.isDropdown = true;
            return;
        }
    }

    getListOfServices() {
        return this.serviceService.getListOfServices()
    }
}