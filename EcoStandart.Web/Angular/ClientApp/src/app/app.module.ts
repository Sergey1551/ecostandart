import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AgmCoreModule} from '@agm/core'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [    
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDsLK_XVBPkvv5NKpy1LBJ1tN5aGjsYYiw'
    }),
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      { path: "home", component: HomeComponent},
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path:'**', redirectTo: '/home'},
    ])
  ],
  exports: [RouterModule,],
  providers: [DeviceDetectorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
