export interface UserRequest {
    name: string;
    email: string;
    phoneNumber: string;
    requestSubject: string;
}
