import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserRequest } from 'src/app/shared/interfaces/user-request'

@Injectable({
    providedIn: 'root'
  })

  export class UserRequestService {

    constructor(private http: HttpClient) { }

    private userRequestUrl = environment.userRequestUrl;

    addUserRequest(userRequest: UserRequest)  : Observable<UserRequest> {

        return this.http.post<UserRequest>(this.userRequestUrl + "addUserRequest", userRequest);
    }
  }