import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ServiceList } from '../interfaces/service-list';

@Injectable({
    providedIn: 'root'
  })

  export class ServiceService {

    constructor(private http: HttpClient) { }

    private serviceUrl = environment.serviceUrl;

    getListOfServices()  : Observable<ServiceList[]> {

        return this.http.get<ServiceList[]>(this.serviceUrl + "getListOfServices");
    }
  }